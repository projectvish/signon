#SignOn#

**SignOn** is a Demo android app developed to record user information along with Signature and store the data in local database
SignOn displays the list of Registered Users from the local database.

### What is this repository for? ###

This repository is for Android SignOn assignment

### What does the App do? ###

•	Allows the user to Join SignOn by entering User information and capturing Signature

•	Data is stored in a local database

•	Data is displayed in chronological order and user can filter the data to display

### Requirements: ###

1.Import project into Android Studio

2.Open the SDK manager to install the required Android SDK Tools and Android SDK Build-tools.

3.There are no Third party libraries or Gradle dependencies used.

4.minSdkVersion 14   targetSdkVersion 23

5.Build the Project.

6.Compile and Run

### Configuration ###

Android Studio IDE, SQLite, Genymotion (Simulator)


***Assumptions***:

The SignOn App works on Portrait Orientation.

The Sorting of user data works fine but is case-sensitive.

The Date time recorded is in the Local date and time format. This is not 
recored in UTC format since it is not in Server