package com.projectvish.signon;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.IOException;

/*
* MainActivity displays the existing SignOn User details
 * in a list
 *
 * You can create new ones via the ActionBar entry "Join"
 */
public class MainActivity extends AppCompatActivity {

    private SignatureDbHelper mSignatureDbHelper;
    private SignatureData mSignatureData;
    private ListView mListView;
    private Cursor mCursor;
    private ImageCursorAdapter mCursorAdapter;
    private Spinner mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSignatureDbHelper = new SignatureDbHelper(this);
        try {
            mSignatureDbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mListView = (ListView)findViewById(android.R.id.list);
        mSpinner = (Spinner)findViewById(R.id.spinner);
        mSignatureData = new SignatureData(this);

        //Creating Adapter for Spinner
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.sign_array, android.R.layout.simple_spinner_item);
        //Set the layout to use for each dropdown item
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Attaching data to spinner
        mSpinner.setAdapter(arrayAdapter);

        //// Columns from DB to map into the View file
        String[] mRecord = { SignatureDbHelper.COLUMN_FNAME, SignatureDbHelper.COLUMN_LNAME,
                SignatureDbHelper.COLUMN_DATE, SignatureDbHelper.COLUMN_IMG};
        // View IDs to map the columns (fetched above) into
        int[] mData = {R.id.fnameView, R.id.lnameView, R.id.dateView, R.id.signImage};


        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                mCursor = mSignatureData.queryData(item);
                mCursorAdapter.updateCursor(mCursor);
                mCursorAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mCursor = mSignatureData.queryAllData();
                mCursorAdapter.updateCursor(mCursor);
                mCursorAdapter.notifyDataSetChanged();
            }
        });

        mCursorAdapter = new ImageCursorAdapter(
                this,               //Context
                R.layout.row_main,  //Layout File
                mCursor,            //DB cursor
                mRecord,            //Data to Bind the UI
                mData);             //Views that will represent the Data from mRecord

        mListView.setAdapter(mCursorAdapter);//Create the List View and Bind the adapter

        // mCursorAdapter is available in the helper methods below and the data will be
        // updated based on action menu interactions
    }

    //Create the Menu based on the XML definition
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    //Menu Selection to Join SignOn
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_join:
                navigateToJoin();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        mCursor = mSignatureData.queryAllData();
        mCursorAdapter.updateCursor(mCursor);
        mCursorAdapter.notifyDataSetChanged();

    }

    //Navigates to Join Activity
    private void navigateToJoin() {
        Intent intent = new Intent(this, JoinActivity.class);
        startActivity(intent);
    }
}
