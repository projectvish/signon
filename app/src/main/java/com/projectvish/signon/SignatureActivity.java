package com.projectvish.signon;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;


public class SignatureActivity extends AppCompatActivity {

    Button clear,save;
    LinearLayout mContent;
    SignatureView mSignatureView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        save = (Button)findViewById(R.id.save);
        save.setEnabled(false);
        clear = (Button)findViewById(R.id.clear);
        mContent = (LinearLayout)findViewById(R.id.mysignature);

        mSignatureView = new SignatureView(this, null);
        mContent.addView(mSignatureView);

        save.setOnClickListener(onButtonClick);
        clear.setOnClickListener(onButtonClick);
    }

    Button.OnClickListener onButtonClick = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            if (v == clear) {
                mSignatureView.clearSignature();
            } else if (v == save) {
                mSignatureView.saveSignature();
                finish();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * The View where the signature will be drawn
     */
    public class SignatureView extends View {

        //set the stroke width
        private static final float STROKE_WIDTH = 10f;
        private static final float HALF_STROKE_WIDTH= STROKE_WIDTH / 2;

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        Paint paint = new Paint();
        Path path = new Path();

        public SignatureView(Context context, AttributeSet attrs){
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);

            //set the background color as White
            this.setBackgroundColor(Color.WHITE);
        }

        //Clear signature canvas
        public void clearSignature(){
            path.reset();
            invalidate();
            save.setEnabled(false);
        }

        //Function to Capture Signature and convert Bitmap to Byte format
        public void saveSignature(){
            Bitmap signatureBitmap = Bitmap.createBitmap(mContent.getWidth(),
                    mContent.getHeight(), Bitmap.Config.RGB_565);

            // Important for saving signature
            Canvas canvas = new Canvas(signatureBitmap);
            Drawable bgDrawable = mContent.getBackground();
            if(bgDrawable != null){
                bgDrawable.draw(canvas);
            }
            else
                canvas.drawColor(Color.WHITE);
            mContent.draw(canvas);

            //Convert Bitmap to Byte format
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Intent intent = new Intent();
            intent.putExtra("byteArray", out.toByteArray());
            setResult(1, intent);
        }

        //All touch events during the Drawing
        @Override
        protected void onDraw(Canvas canvas){
            canvas.drawPath(this.path, this.paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event){
            float eventX = event.getX();
            float eventY = event.getY();
            save.setEnabled(true);

            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_MOVE:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);

                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom - HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;
            return true;
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
