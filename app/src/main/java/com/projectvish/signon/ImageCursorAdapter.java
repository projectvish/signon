package com.projectvish.signon;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vish on 14/12/15.
 */
public class ImageCursorAdapter extends SimpleCursorAdapter{
    public Cursor cursor;
    private Context context;

    public ImageCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to, 0);
        this.cursor = c;
        this.context = context;
    }

    public View getView(int pos, View inView, ViewGroup parent) {
        View v = inView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_main, null);
        }

        this.cursor.moveToPosition(pos);
        //Populate View for each Data
        String firstName = this.cursor.getString(this.cursor.getColumnIndex("firstname"));
        String lastName = this.cursor.getString(this.cursor.getColumnIndex("lastname"));
        String dateStr = this.cursor.getString(this.cursor.getColumnIndex("date"));
         byte[] image = this.cursor.getBlob(this.cursor.getColumnIndex("image"));

        TextView fname = (TextView) v.findViewById(R.id.fnameView);
        fname.setText(firstName);

        TextView lname = (TextView) v.findViewById(R.id.lnameView);
        lname.setText(lastName);

        TextView dtView = (TextView) v.findViewById(R.id.dateView);
        //Convert Date Format
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
        try {
            Date date = format.parse(dateStr);
            dtView.setText(newFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Convert Byte Image to Bitmap
        ImageView iv = (ImageView) v.findViewById(R.id.signImage);
        if (image != null) {
            iv.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
        }
        return (v);
    }

    public void updateCursor(Cursor c) {
        this.cursor = c;
        super.changeCursor(c);
    }
}

