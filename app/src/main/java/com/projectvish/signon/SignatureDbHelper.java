package com.projectvish.signon;


import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.IOException;


/**
 * Created by Vish on 10/12/15.
 */
public class SignatureDbHelper extends SQLiteOpenHelper {

    //Database Definition
    //private static final String DATABASE_NAME = "signsample.db";
    private static String DB_PATH = "/data/data/com.projectvish.signon/databases/";
    private static String DB_NAME = "signon.sqlite";
    private static final int DATABASE_VERSION = 1;


    //Database Table definition
    public static final String TABLE_NAME = "signature";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FNAME = "firstname";
    public static final String COLUMN_LNAME = "lastname";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_IMG = "image";

    //SQL Statment for Database creation
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_FNAME + " text, "
            + COLUMN_LNAME + " text, "
            + COLUMN_DATE + " text, "
            + COLUMN_IMG + " blob "
            + ");";

    public SignatureDbHelper(Context context){
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    public synchronized void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }
        else {
            this.getWritableDatabase();
        }
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;

            if (new File(myPath).exists()) {
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            }
        }
        catch(SQLException e) {
            //database does't exist yet.
        }
        catch(Exception e) {
        }

        if(checkDB != null) {
            checkDB.close();
            return true;
        }

        return false;
    }

    //Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    // Method is called during an upgrade of the database,
    // e.g. if you increase the database version
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(this.getDatabaseName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

}
