package com.projectvish.signon;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class JoinActivity extends AppCompatActivity {
    protected SignatureData mSignatureData;
    protected EditText mFirstname;
    protected EditText mLastname;
    protected Button mJoinButton;
    protected Button mGetSign;
    protected ImageView mSignImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mSignatureData = new SignatureData(this);

        mFirstname = (EditText)findViewById(R.id.firstnameField);
        mLastname = (EditText)findViewById(R.id.lastnameField);
        mGetSign = (Button)findViewById(R.id.getSign);
        mSignImage = (ImageView)findViewById(R.id.signImageView);
        mGetSign.setOnClickListener(onButtonClick);

        mJoinButton = (Button)findViewById(R.id.btnJoin);
        mJoinButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String firstname = mFirstname.getText().toString();
                String lastname = mLastname.getText().toString();

            //Form Validation Check
                if(firstname.isEmpty() || lastname.isEmpty() || mSignImage.getDrawable() == null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(JoinActivity.this);
                    builder.setMessage(R.string.join_error_message)
                            .setTitle(R.string.join_error_title)
                            .setPositiveButton(android.R.string.ok,null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else if (firstname.length() > 50){
                    AlertDialog.Builder builder = new AlertDialog.Builder(JoinActivity.this);
                    builder.setMessage(R.string.uservalidation_error_message)
                            .setTitle(R.string.uservalidation_error_title)
                            .setPositiveButton(android.R.string.ok,null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else if (lastname.length() > 50){
                    AlertDialog.Builder builder = new AlertDialog.Builder(JoinActivity.this);
                    builder.setMessage(R.string.surnamevalidation_error_message)
                            .setTitle(R.string.uservalidation_error_title)
                            .setPositiveButton(android.R.string.ok,null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    Bitmap image = ((BitmapDrawable)mSignImage.getDrawable()).getBitmap();
                    lastname = lastname.trim();
                    firstname = firstname.trim();
                    boolean isInserted = mSignatureData.insertData(firstname, lastname, image);
                    if (isInserted) {
                            //Data Insertion is Successful
                            Toast.makeText(JoinActivity.this, "SignOn Successful", Toast.LENGTH_LONG).show();
                            finish();
                    } else {
                            //Data Insertion failed
                            Toast.makeText(JoinActivity.this, "SignOn Failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    Button.OnClickListener onButtonClick =  new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(JoinActivity.this, SignatureActivity.class);
            startActivityForResult(intent, 0);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == 1) {
            Bitmap b = BitmapFactory.decodeByteArray(
                    data.getByteArrayExtra("byteArray"), 0,
                    data.getByteArrayExtra("byteArray").length);
            mSignImage.setImageBitmap(b);
        }
    }
}
