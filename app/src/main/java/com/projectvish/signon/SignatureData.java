package com.projectvish.signon;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.ParseException;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vish on 13/12/15.
 */
public class SignatureData {
    SQLiteDatabase database;
    SignatureDbHelper mDbHelper;
    Context context;

    public SignatureData(Context c){
        context = c;
    }

    //Open database in Read Mode
    public SignatureData openToRead(){
        mDbHelper = new SignatureDbHelper(context);
        database = mDbHelper.getReadableDatabase();
        return this;
    }

    //Open Database in Write Mode
    public SignatureData openToWrite(){
        mDbHelper = new SignatureDbHelper(context);
        database = mDbHelper.getWritableDatabase();
        return this;
    }

    public void Close(){
        database.close();
    }

    //Insertion of Data into Database
    public boolean insertData(String firstname, String lastname, Bitmap image){
        String datetime = null;

        //Convert Date to String value to insert into Database
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = new Date();
            datetime = dateFormat.format(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bArray = bos.toByteArray();

        ContentValues contentValues = new ContentValues();
        contentValues.put(SignatureDbHelper.COLUMN_FNAME, firstname);
        contentValues.put(SignatureDbHelper.COLUMN_LNAME, lastname);
        contentValues.put(SignatureDbHelper.COLUMN_DATE, datetime);
        contentValues.put(SignatureDbHelper.COLUMN_IMG, bArray);
        openToWrite();
        long result = database.insert(SignatureDbHelper.TABLE_NAME, null, contentValues);
        Close();
        if (result == -1){
            return false;
        }
        else{
            return true;
        }
    }

    public Cursor queryAllData(){
        String[] cols = {SignatureDbHelper.COLUMN_ID, SignatureDbHelper.COLUMN_FNAME,
                SignatureDbHelper.COLUMN_LNAME, SignatureDbHelper.COLUMN_DATE, SignatureDbHelper.COLUMN_IMG};
        openToWrite();
        Cursor cursor = database.query(SignatureDbHelper.TABLE_NAME, cols, null, null, null, null, null);
        return cursor;
    }

    public Cursor queryData(String item){
        String[] cols = {SignatureDbHelper.COLUMN_ID, SignatureDbHelper.COLUMN_FNAME,
                SignatureDbHelper.COLUMN_LNAME, SignatureDbHelper.COLUMN_DATE, SignatureDbHelper.COLUMN_IMG};
        openToWrite();
        Cursor cursor1 = database.query(SignatureDbHelper.TABLE_NAME, cols, null, null, null, null, item, null);
        return cursor1;
    }

}
